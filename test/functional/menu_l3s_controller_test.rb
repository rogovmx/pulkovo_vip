require 'test_helper'

class MenuL3sControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:menu_l3s)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create menu_l3" do
    assert_difference('MenuL3.count') do
      post :create, :menu_l3 => { }
    end

    assert_redirected_to menu_l3_path(assigns(:menu_l3))
  end

  test "should show menu_l3" do
    get :show, :id => menu_l3s(:one).to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => menu_l3s(:one).to_param
    assert_response :success
  end

  test "should update menu_l3" do
    put :update, :id => menu_l3s(:one).to_param, :menu_l3 => { }
    assert_redirected_to menu_l3_path(assigns(:menu_l3))
  end

  test "should destroy menu_l3" do
    assert_difference('MenuL3.count', -1) do
      delete :destroy, :id => menu_l3s(:one).to_param
    end

    assert_redirected_to menu_l3s_path
  end
end
