require 'test_helper'

class PressrelizsControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:pressrelizs)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create pressreliz" do
    assert_difference('Pressreliz.count') do
      post :create, :pressreliz => { }
    end

    assert_redirected_to pressreliz_path(assigns(:pressreliz))
  end

  test "should show pressreliz" do
    get :show, :id => pressrelizs(:one).to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => pressrelizs(:one).to_param
    assert_response :success
  end

  test "should update pressreliz" do
    put :update, :id => pressrelizs(:one).to_param, :pressreliz => { }
    assert_redirected_to pressreliz_path(assigns(:pressreliz))
  end

  test "should destroy pressreliz" do
    assert_difference('Pressreliz.count', -1) do
      delete :destroy, :id => pressrelizs(:one).to_param
    end

    assert_redirected_to pressrelizs_path
  end
end
