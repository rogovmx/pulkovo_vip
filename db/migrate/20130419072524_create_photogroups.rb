class CreatePhotogroups < ActiveRecord::Migration
  def self.up
    create_table :photogroups do |t|
      t.string :title
      t.text :notice
      t.string :title_en
      t.text :notice_en
      
      t.timestamps
    end
  end

  def self.down
    drop_table :photogroups
  end
end
