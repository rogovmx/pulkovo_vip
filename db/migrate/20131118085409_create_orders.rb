class CreateOrders < ActiveRecord::Migration
  def self.up
    create_table :orders do |t|
      t.string :company
      t.string :m_phone
      t.string :fax
      t.string :mail
      t.string :person
      t.string :departure
      t.string :destination
      t.string :date
      t.string :flight_num
      t.string :flight_time
      t.string :arrival_time
      t.string :city
      t.string :cars
      t.string :payment
      t.text :dop

      t.timestamps
    end
  end

  def self.down
    drop_table :orders
  end
end
