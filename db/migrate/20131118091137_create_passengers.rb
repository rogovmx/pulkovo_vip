class CreatePassengers < ActiveRecord::Migration
  def self.up
    create_table :passengers do |t|
      t.integer :order_id
      t.string :name
      t.string :birthdate
      t.timestamps
    end
  end

  def self.down
    drop_table :passengers
  end
end
