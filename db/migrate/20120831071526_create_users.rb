class CreateUsers < ActiveRecord::Migration
  def self.up
    create_table :users do |t|
      t.string :mail
      t.string :fname
      t.string :pname
      t.string :sname
      t.string :phone
      t.string :company
      t.string :hashed_password
      t.string :salt
      t.integer :email_confirm
      t.boolean :spam
      t.string :role
      t.string :city

      t.timestamps
    end
  end

  def self.down
    drop_table :users
  end
end
