class CreateMenuL1s < ActiveRecord::Migration
  def self.up
    create_table :menu_l1s do |t|
      t.string :title
      t.string :url
      t.text :content
      t.boolean :vis
      t.timestamps
    end
  end

  def self.down
    drop_table :menu_l1s
  end
end
