class CreateMenuL2s < ActiveRecord::Migration
  def self.up
    create_table :menu_l2s do |t|
      t.string :title
      t.string :url
      t.text :content
      t.boolean :vis
      t.integer :menu_l1_id

      t.timestamps
    end
  end

  def self.down
    drop_table :menu_l2s
  end
end
