class CreateNewsItems < ActiveRecord::Migration
  def self.up
    create_table :news_items do |t|
      t.string :title
      t.text :notice
      t.text :content
      t.string :title_en
      t.text :notice_en
      t.text :content_en

      t.timestamps
    end
  end

  def self.down
    drop_table :news_items
  end
end
