class CreateMarketingConsultations < ActiveRecord::Migration
  def self.up
    create_table :marketing_consultations do |t|
      t.string :name
      t.string :email
      t.string :phone

      t.timestamps
    end
  end

  def self.down
    drop_table :marketing_consultations
  end
end
