class CreateConfsubs < ActiveRecord::Migration
  def self.up
    create_table :confsubs do |t|

      t.timestamps
    end
  end

  def self.down
    drop_table :confsubs
  end
end
