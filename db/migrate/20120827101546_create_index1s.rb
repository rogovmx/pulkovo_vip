class CreateIndex1s < ActiveRecord::Migration
  def self.up
    create_table :index1s do |t|
      t.text :content

      t.timestamps
    end
  end

  def self.down
    drop_table :index1s
  end
end
