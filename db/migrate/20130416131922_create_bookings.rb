class CreateBookings < ActiveRecord::Migration
  def self.up
    create_table :bookings do |t|
      t.string :name
      t.string :email
      t.string :phone
      t.string :actions_number

      t.timestamps
    end
  end

  def self.down
    drop_table :bookings
  end
end
