ActionController::Routing::Routes.draw do |map|
  map.resources :orders

  
  map.connect '/spams/all_emails', :controller => 'spams', :action => 'all_emails'
  map.resources :photos
  map.resources :orders

  map.resources :smis


  map.resources :news_items

  
  map.resources :spams
  
  map.resources :users

  map.resources :menu_l3s
  map.resources :menu_l1s
  map.resources :menu_l2s

  # The priority is based upon order of creation: first created -> highest priority.

  # Sample of regular route:
  #   map.connect 'products/:id', :controller => 'catalog', :action => 'view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   map.purchase 'products/:id/purchase', :controller => 'catalog', :action => 'purchase'
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   map.resources :products

  # Sample resource route with options:
  #   map.resources :products, :member => { :short => :get, :toggle => :post }, :collection => { :sold => :get }

  # Sample resource route with sub-resources:
  #   map.resources :products, :has_many => [ :comments, :sales ], :has_one => :seller
  
  # Sample resource route with more complex sub-resources
  #   map.resources :products do |products|
  #     products.resources :comments
  #     products.resources :sales, :collection => { :recent => :get }
  #   end

  # Sample resource route within a namespace:
  #   map.namespace :admin do |admin|
  #     # Directs /admin/products/* to Admin::ProductsController (app/controllers/admin/products_controller.rb)
  #     admin.resources :products
  #   end

  # You can have the root of your site routed with map.root -- just remember to delete public/index.html.
  map.root :controller => "main"

  map.connect 'main/news/:id', :controller => 'main', :action => 'news_item'
  map.connect 'photos/new_group', :controller => 'photos', :action => 'new_group' 
  # See how all your routes lay out with "rake routes"

  # Install the default routes as the lowest priority.
  # Note: These default routes make all actions in every controller accessible via GET requests. You should
  # consider removing or commenting them out if you're using named routes and resources.
  map.connect ':controller/:action/:id' #, :requirement=>{:id =>/d/}
  map.connect ':controller/:action/:id.:format'
  #map.connect "/:str" ,:controller=>'main',:action=>'page', :requirement=>{:str =>/[а-я]/},:str=>nil
  #map.connect "/:str/:str2" ,:controller=>'main',:action=>'page', :requirement=>{:str =>/[а-я]/ , :str2 =>/[а-я]/ },:str=>nil ,:str2=>nil
  map.connect "/:str/:str2/:str3" ,:controller=>'main',:action=>'page', :requirement=>{:str =>/[a-z]/ , :str2 =>/[a-z]/ , :str3 =>/[a-z]/ },:str=>nil ,:str2=>nil ,:str3=>nil
  map.connect '/',:controller => 'main', :action=>'index'
  

end
