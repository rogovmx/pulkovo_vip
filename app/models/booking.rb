class Booking < ActiveRecord::Base
  validates_presence_of :name, :message=>"Поле ФИО не может быть пустым."
  validates_presence_of :email, :message=>"Поле E-mail не может быть пустым."
  validates_presence_of :phone, :message=>"Поле Телефон не может быть пустым."
  validates_presence_of :actions_number, :message=>"Поле количество акций не может быть пустым."
end
