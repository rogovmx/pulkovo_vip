class MenuL2sController < ApplicationController
  layout 'admin'

  before_filter :authorize
before_filter :redaktor

#  def index
#    @menu_l2s = MenuL2.all
#
#    respond_to do |format|
#      format.html # index.html.erb
#      format.xml  { render :xml => @menu_l2s }
#    end
#  end

# def show
#
#    @menu_l2 = MenuL2.find(params[:id])
#
#    respond_to do |format|
#      format.html # show.html.erb
#      format.xml  { render :xml => @menu_l2 }
#    end
#  end

  # GET /menu_l2s/new
  # GET /menu_l2s/new.xml
  def new
    @menu_l2 = MenuL2.new
    @menu_l2.menu_l1_id = params[:id].to_i
    @upmenu = MenuL1.find_by_id(@menu_l2.menu_l1_id)
    @menu = MenuL1.all
    @page_title = "Администрирование. Пулково VIP."
    
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @menu_l2 }
    end
  end

  # GET /menu_l2s/1/edit
  def edit
    @menu_l2 = MenuL2.find(params[:id])
    @upmenu = MenuL1.find_by_id(@menu_l2.menu_l1_id)
    @menu = MenuL1.all
    @page_title = "Администрирование. Пулково VIP."
    
  end

  # POST /menu_l2s
  # POST /menu_l2s.xml
  def create
    @menu_l2 = MenuL2.new(params[:menu_l2])

    respond_to do |format|
      if @menu_l2.save
        format.html { redirect_to(:controller=>'admin') }
        format.xml  { render :xml => @menu_l2, :status => :created, :location => @menu_l2 }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @menu_l2.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /menu_l2s/1
  # PUT /menu_l2s/1.xml
  def update
    @menu_l2 = MenuL2.find(params[:id])

    respond_to do |format|
      if @menu_l2.update_attributes(params[:menu_l2])
        format.html { redirect_to(:controller=>'admin') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @menu_l2.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /menu_l2s/1
  # DELETE /menu_l2s/1.xml
  def del
    @menu_l2 = MenuL2.find(params[:id])
    @menu_l2.destroy
    redirect_to(:controller=>'admin')
    

  end
end
