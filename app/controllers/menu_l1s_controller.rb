class MenuL1sController < ApplicationController
  layout 'admin'
  # GET /menu_l1s
  # GET /menu_l1s.xml

before_filter :authorize
before_filter :redaktor



  # GET /menu_l1s/1/edit
  def edit
    @menu = MenuL1.all
    @page_title = "Администрирование. Пулково VIP."
    @menu_l1 = MenuL1.find(params[:id])
  end

  def update
    @menu_l1 = MenuL1.find(params[:id])

    respond_to do |format|
      if @menu_l1.update_attributes(params[:menu_l1])
        format.html { redirect_to(:controller=>'admin') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @menu_l1.errors, :status => :unprocessable_entity }
      end
    end
  end


end
