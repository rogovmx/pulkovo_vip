class MenuL3sController < ApplicationController
  layout 'admin'

before_filter :authorize
before_filter :redaktor

  def new
    @menu_l3 = MenuL3.new
    @menu_l3.menu_l2_id = params[:id].to_i
    @upmenu2 = MenuL2.find_by_id(@menu_l3.menu_l2_id)
    @upmenu = MenuL1.find_by_id(@upmenu2.menu_l1_id)

    @menu = MenuL1.all
    @page_title = "Администрирование. Пулково VIP"

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @menu_l3 }
    end
  end

  # GET /menu_l3s/1/edit
  def edit
    @menu_l3 = MenuL3.find(params[:id])
    @upmenu2 = MenuL2.find_by_id(@menu_l3.menu_l2_id)
    @upmenu = MenuL1.find_by_id(@upmenu2.menu_l1_id)

    @menu = MenuL1.all
    @page_title = "Администрирование. Пулково VIP"

  end

  # POST /menu_l3s
  # POST /menu_l3s.xml
  def create
    @menu_l3 = MenuL3.new(params[:menu_l3])

    respond_to do |format|
      if @menu_l3.save
        format.html { redirect_to(:controller=>'admin') }
        format.xml  { render :xml => @menu_l3, :status => :created, :location => @menu_l3 }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @menu_l3.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /menu_l3s/1
  # PUT /menu_l3s/1.xml
  def update
    @menu_l3 = MenuL3.find(params[:id])

    respond_to do |format|
      if @menu_l3.update_attributes(params[:menu_l3])
        format.html { redirect_to(:controller=>'admin')}
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @menu_l3.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /menu_l3s/1
  # DELETE /menu_l3s/1.xml
  def del
    @menu_l3 = MenuL3.find(params[:id])
    @menu_l3.destroy
    redirect_to(:controller=>'admin')

  end
end
