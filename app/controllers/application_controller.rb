# Filters added to this controller apply to all controllers in the application.
# Likewise, all the methods added will be available for all controllers.

class ApplicationController < ActionController::Base
  helper :all # include all helpers, all the time
  protect_from_forgery # See ActionController::RequestForgeryProtection for details
#  response.headers["Content-Type"] = 'text/html'
  # Scrub sensitive parameters from your log
  # filter_parameter_logging :password

#   before_filter :set_charsets
#
#
#  def set_charset
#    if request.xml?
#    @headers["Content-Type"] = "text/xml; charset=utf-8"
#    else @headers["Content-Type"] = "text/html; charset=utf-8"
#    end
#  end

def user
    @user = User.find_by_id(session[:user_id])

end

 def menu
  @menu = MenuL1.visible.sort{|a, b| a.ord <=> b.ord}
 end

   def authorize
     unless User.find_by_id(session[:user_id])
     #  session[:original_uri]=request.request_uri
     flash[:notice] = 'Вы не авторизованы.'
     redirect_to(:controller=>'login',:action=>'login')
     end
   end



   def redaktor
      @user=User.find_by_id(session[:user_id])
      unless (@user.role=='redaktor' or @user.role=='sredaktor')
        flash[:notice] = 'У вас нет прав редактора.'
        redirect_to :controller=>'main'
      end
   end

def ru?
    if session[:lang] == 'ru' or session[:lang] == nil
      return true
    else
      return false
    end
end

end
