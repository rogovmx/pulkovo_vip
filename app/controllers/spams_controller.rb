class SpamsController < ApplicationController

before_filter :authorize
before_filter :redaktor

layout 'admin'

  def send_spam
    @spam = Spam.find(params[:id])

    @addr = Subscribe.find :all , :conditions=>['spam =?',1]
    unless params[:test]
    for addr in @addr do
    Mailer.deliver_spam(@spam , addr.mail , addr.conf)
    end
      @spam.delivered = 1
      @spam.save
      render :update do |page|
      page.replace_html('send_spam' , :text=>'Рассылка отправлена!')
      end
    else
    Mailer.deliver_spam(@spam , 'mkolupaeva@spens.ru' , 1)
      render :update do |page|
      page.replace_html('send_spam2' , :text=>'Тестовая рассылка отправлена!')
      end
    end
  end

  # GET /spams
  # GET /spams.xml
  def index
    @addr = Subscribe.find :all , :conditions=>['spam =?',1]
    @spams = Spam.all
    @photos = Photo.all
     menu
    @page_title = "Администрирование. Рассылки."

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @spams }
    end
  end

  # GET /spams/1
  # GET /spams/1.xml
  def show
    @addr = Subscribe.find :all , :conditions=>['spam =?',1]
    @spam = Spam.find(params[:id])
    @photos = Photo.all
     menu
    @page_title = "Администрирование. Рассылки."
    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @spam }
    end
  end
  
  def all_emails
    @subscriptions = Subscribe.find :all , :conditions=>['spam =?',1]
    @menu = MenuL1.all.sort{|a,b| a <=> b}
    respond_to do |format|
      format.html # all_emails.html.erb
      format.xml  { render :xml => @subscriptions }
    end
  end
  # GET /spams/new
  # GET /spams/new.xml
  def new
    @spam = Spam.new
    @photos = Photo.all
    @menu = MenuL1.all.sort{|a,b| a <=> b}
    @page_title = "Администрирование. Рассылки."
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @spam }
    end
  end

  # GET /spams/1/edit
  def edit
    @spam = Spam.find(params[:id])
    @photos = Photo.all
    @menu = MenuL1.all.sort{|a,b| a <=> b}
    @page_title = "Администрирование. Рассылки."
  end

  # POST /spams
  # POST /spams.xml
  def create
    @spam = Spam.new(params[:spam])

    respond_to do |format|
      if @spam.save
        format.html { redirect_to(@spam, :notice => 'Рассылка создана.') }
        format.xml  { render :xml => @spam, :status => :created, :location => @spam }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @spam.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /spams/1
  # PUT /spams/1.xml
  def update
    @spam = Spam.find(params[:id])

    respond_to do |format|
      if @spam.update_attributes(params[:spam])
        format.html { redirect_to(@spam, :notice => 'Рассылка сохранена.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @spam.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /spams/1
  # DELETE /spams/1.xml
  def destroy
    @spam = Spam.find(params[:id])
    @spam.destroy

    respond_to do |format|
      format.html { redirect_to(spams_url) }
      format.xml  { head :ok }
    end
  end
end
