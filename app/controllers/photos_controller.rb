class PhotosController < ApplicationController
before_filter :authorize
before_filter :redaktor
layout 'admin'
  def index

    @photos_groups = Photogroup.all

    @menu = MenuL1.all
    @page_title = "Администрирование. Фотобанк"

  end

  def index2
    @gruop = Photogroup.find(params[:id])
    @photos = @gruop.photo

     menu
    @page_title = "Администрирование. Фотобанк. <br/> Группа: " + @gruop.title

  end

  # GET /photos/1
  # GET /photos/1.xml
  def show
    @photo = Photo.find(params[:id])
     menu
    @page_title = "Администрирование. Фотобанк"
    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @photo }
    end
  end

  # GET /photos/new
  # GET /photos/new.xml
  def new
    @photo = Photo.new
     menu
    @page_title = "Администрирование. Фотобанк"

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @photo }
    end
  end

  def new_group
   
    @menu = MenuL1.all.sort{|a,b| a <=> b}
    @page_title = "Администрирование. Фотобанк<br/> Новая группа."


  end

  def edit_group

    @menu = MenuL1.all.sort{|a,b| a <=> b}
    @photogroup = Photogroup.find(params[:id])
    @page_title = "Администрирование. Фотобанк<br/> Редактирование группа."
  end

  # GET /photos/1/edit
  def edit
    @menu = MenuL1.all.sort{|a,b| a <=> b}
    @page_title = "Администрирование. Фотобанк"
    @photo = Photo.find(params[:id])
  end

  # POST /photos
  # POST /photos.xml



  def create
     new_photo(params[:photo])
     redirect_to :action=>'index2' , :id =>@photo.photogroup_id
  end

  def create_group
     @pg = Photogroup.new(params[:photogroup])
     if @pg.save
       flash[:notice] = 'Группа сохранена'
       redirect_to :action=>'index'
     end
  end

  def update_group
    @pg = Photogroup.find(params[:id])
    if @pg.update_attributes(params[:photogroup])
    flash[:notice] = 'Группа изменена'
    redirect_to :action=>'index'
    else
    flash[:notice] = 'Группа не сохранена'
    redirect_to :action=>'edit_group' , :id => @pg.id
    end
  end


  def update_photo
    @photo = Photo.find(params[:id])
    edit_photo(params[:photo], params[:id])
    redirect_to :action=>'index2' , :id =>@photo.photogroup_id
  end

  # DELETE /photos/1
  # DELETE /photos/1.xml
  def destroy
    @photo = Photo.find(params[:id])
    @photo.destroy

    respond_to do |format|
      format.html { redirect_to(photos_url) }
      format.xml  { head :ok }
    end
  end


  def del_group
    @pg = Photogroup.find(params[:id])
    @pg.destroy

    respond_to do |format|
      format.html { redirect_to(photos_url) }
      format.xml  { head :ok }
    end
  end

   def new_photo(upload)
    name_prw =  upload[:prw].original_filename if upload[:prw]
    name_img =  upload[:img].original_filename if upload[:img]
    directory = 'public/photo'
    # create the file path
    path_prw = File.join(directory, name_prw) if upload[:prw]
    path_img = File.join(directory, name_img) if upload[:img]
    # write the file
    @photo=Photo.new
    @photo.prw = name_prw
    @photo.img = name_img
    @photo.title = upload[:title]
    @photo.photogroup_id = upload[:photogroup_id]
    @photo.save
   if upload[:prw].instance_of?(Tempfile)
   FileUtils.cp(upload[:prw].local_path, path_prw)
   else
    File.open(path_prw, "wb") do |f|
    f.write(upload[:prw].read)  if upload[:prw]
    end
   end if upload[:prw]

   
   if upload[:img].instance_of?(Tempfile)
   FileUtils.cp(upload[:img].local_path, path_img)
   else
   File.open(path_img, "wb") do |f|
   f.write(upload[:img].read)  if upload[:img]
   new_permission = f.lstat(path_img).mode ^ 0004
   f.chmod(new_permission, path_img)
   end
   end if upload[:img]

   if upload[:prw]
   new_permission = File.lstat(path_prw).mode | 0444 
   File.chmod(new_permission, path_prw)
   end
   if upload[:img]
   new_permission = File.lstat(path_img).mode | 0444 
   File.chmod(new_permission, path_img)
   end

  end

def edit_photo(upload, id)
    name_prw =  upload[:prw].original_filename if upload[:prw]
    name_img =  upload[:img].original_filename if upload[:img]
    directory = 'public/photo'
    # create the file path
    path_prw = File.join(directory, name_prw) if upload[:prw]
    path_img = File.join(directory, name_img) if upload[:img]
    # write the file
    @photo=Photo.find(id)
    @photo.prw = name_prw if upload[:prw]
    @photo.img = name_img if upload[:img]
    @photo.title = upload[:title]
    @photo.save
   if upload[:prw].instance_of?(Tempfile)
   FileUtils.cp(upload[:prw].local_path, path_prw)
   else
   File.open(path_prw, "wb") { |f| f.write(upload[:prw].read) } if upload[:prw]
   end if upload[:prw]

   if upload[:img].instance_of?(Tempfile)
   FileUtils.cp(upload[:img].local_path, path_img)
   else
   File.open(path_img, "wb") { |f| f.write(upload[:img].read) } if upload[:img]
   end if upload[:img]

   if upload[:prw]
   new_permission = File.lstat(path_prw).mode | 0444 
   File.chmod(new_permission, path_prw)
   end
   if upload[:img]
   new_permission = File.lstat(path_img).mode | 0444 
   File.chmod(new_permission, path_img)
   end
end


end
