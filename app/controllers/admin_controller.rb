class AdminController < ApplicationController

before_filter :authorize
before_filter :redaktor

def initialize
@menu = MenuL1.all.sort{|a,b| a.ord <=> b.ord}
end

  def index
    @title = "Дерево сайта."
    @page_title = "Администрирование. "
    if params[:id]
    @edit_menu = MenuL1.find(:all , :conditions=>['id=?',params[:id]] , :order=> 'ord')
    @title += " Меню #{@edit_menu[0].title}"
    else
    @edit_menu = MenuL1.all.sort{|a,b| a.ord <=> b.ord}
    end
  end

  def main_page
    
    @page_title = "Администрирование. Главная страница"
    @content = Index1.find(1).content
    @index = Index1.find(1)
  end

  def save_index
    
    @index = Index1.find(1)
    if @index.update_attributes(params[:index])
     # render :text => params[:index]
      redirect_to(:action=>'index', :notice => 'Главная страница сохранена')
    end

  end



end
