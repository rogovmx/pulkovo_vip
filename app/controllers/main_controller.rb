class MainController < ApplicationController
  layout 'main'


def initialize
menu
end

def index
    user
   ## if request.xhr? @headers["Content-Type"] = "text/javascript; charset=utf-8" else @headers["Content-Type"] = "text/html; charset=utf-8"
   ## headers["Content-Type"] = "text/html; charset=utf-8"


    @mp = 1

    if ru?
      @page_title = " VIP сервис в аэропортах: вип залы, бизнес-залы ЗОЛД и другие виды vip услуг."
      @content = Index1.find(1).content
    else
      @page_title = " VIP сервис в аэропортах: вип залы, бизнес-залы ЗОЛД и другие виды vip услуг."
      @content = Index1.find(1).content_en
    end
    @news = NewsItem.find :all , :order =>'created_at desc' , :limit => '3'



    render :layout => 'index'



end



def page
    user
    begin
    @menu.map!{|x| x if x.menu_l2.size != 0}
    #@act_menu = MenuL1.find(:first , :conditions=>['url = ? or url_en = ?', params[:str], params[:str]] )
    @act_menu = MenuL1.find(:first , :conditions=>['url = ? ', params[:str]] )
    @act_menu_l2 = @act_menu.menu_l2.sort{|a, b| a.ord <=> b.ord}.map{|x| x if x.vis}[0]

    if @act_menu_l2.menu_l3.size != 0
    @act_menu_l3 = @act_menu_l2.menu_l3.map{|x| x if x.vis}[0]
    @content = @act_menu_l3
    @page_title = @act_menu_l3.title if ru?
    @page_title = @act_menu_l3.title_en unless ru?
    else
    @content = @act_menu_l2
    @page_title = @act_menu_l2.title if ru?
    @page_title = @act_menu_l2.title_en unless ru?
    end

    if params[:str2] #and (params[:str3] == nil)
    #@act_menu_l2 = MenuL2.find(:first , :conditions=>['url = ? or url_en = ? and vis = ? and menu_l1_id = ?', params[:str2], params[:str2] ,1 , @act_menu.id])
    @act_menu_l2 = MenuL2.find(:first , :conditions=>['url = ?  and vis = ? and menu_l1_id = ?', params[:str2] ,1 , @act_menu.id])

      if @act_menu_l2.menu_l3.map{|x| x if x}.size != 0
      @act_menu_l3 = @act_menu_l2.menu_l3.map{|x| x if x.vis}[0]
      @content = @act_menu_l3
      @page_title = @act_menu.title + '. ' + @act_menu_l2.title + '. ' + @act_menu_l3.title if ru?
      @page_title = @act_menu.title_en + '. ' + @act_menu_l2.title_en + '. ' + @act_menu_l3.title_en unless ru?
      else
      @content = @act_menu_l2
      @page_title = @act_menu.title + '. ' + @act_menu_l2.title if ru?
      @page_title = @act_menu.title_en + '. ' + @act_menu_l2.title_en unless ru?
      end
    end

    if params[:str3]
    #@act_menu_l3 = MenuL3.find(:first , :conditions=>['url = ? or url_en = ? and vis = ? and menu_l2_id = ?', params[:str3] , params[:str3], 1 , @act_menu_l2.id])
    @act_menu_l3 = MenuL3.find(:first , :conditions=>['url = ? and vis = ? and menu_l2_id = ?', params[:str3] , 1 , @act_menu_l2.id])

    @content = @act_menu_l3
    @page_title = @act_menu.title + '. ' + @act_menu_l2.title + '. ' + @act_menu_l3.title if ru?
    @page_title = @act_menu.title_en + '. ' + @act_menu_l2.title_en + '. ' + @act_menu_l3.title_en unless ru?
    end
    rescue
    redirect_to '/'
    end

end

def sitetree
  user
  @page_title ="Пулково VIP. Карта сайта " if ru?
  @page_title ="Пулково VIP. Site map " unless ru?
  @title = "Карта сайта" if ru?
  @title = "Site map" unless ru?
  render :layout => 'index'
end


def feedback

  @feedback = Feedback.new(params[:feedback])
  if @feedback.save
  Mailer.deliver_feedback(@feedback)
  flash[:notice] = 'Ваше сообщение отправлено' if ru?
  flash[:notice] = 'Done' unless ru?

  render :partial=>'/modul/feedback'
  end
end


def press
@page_title ="Пулково VIP.Пресс-релизы " if ru?
@page_title ="Пулково VIP. Press releases. " unless ru?
@press = Pressreliz.find_by_id(params[:id])
if @press
@act_menu = MenuL1.find(4)
@act_menu_l2 = MenuL2.find(:first , :conditions=>['url = ?', 'press-releases'])
@title = "Пресс-релиз: #{@press.title}" if ru?
@title = "Press releas: #{@press.title_en}" unless ru?
else
redirect_to '/press/press-releases'
end
end

def smi
  user

  @page_title ="Пулково VIP. СМИ о компании " if ru?
  @page_title ="Пулково VIP. Media about company " unless ru?
  @smi = Smi.find_by_id(params[:id])
  if @smi
    @act_menu = MenuL1.find(4)
    @act_menu_l2 = MenuL2.find(:first , :conditions=>['url = ?', 'СМИ о компании'])
    @title = "СМИ о компании: #{@smi.title}" if ru?
    @title = "Media about company: #{@smi.title_en}" unless ru?
  else
    redirect_to '/press/media'
  end
end

def news_item
  user

  @page_title ="Новости."
  @news_item = NewsItem.find_by_id(params[:id])
  @act_menu = MenuL1.find(6)
  @act_menu_l2 = MenuL2.find(:first , :conditions=>['url = ?', 'news'])

  @title = "Новости: #{@news_item.title}"
end

def chng_lang
  session[:lang] = params[:lang]
  redirect_to :back
end

def update_passenger

@quant = params[:quant].to_i
@out = "<table>
 <tbody><tr><th>№</th>
 <th>Ф.И.О. <font color=\"#ff0000\">*</font></th>
 <th>Дата рождения</th>
  </tr>"

1.upto(@quant) do |i|

   @out += %{<tr>   <td style="WIDTH: 3%">#{i}</td>
   <td style="WIDTH: 80%">
     <input type="text"  name="zakaz[name-#{i}]" id="zakaz_name-#{i}" style="WIDTH: 100%"/>
   </td>
   <td style="WIDTH: 17%">
     <input type="text"  name="zakaz[birthdate-#{i}]" id="zakaz_birthdate-#{i}" style="WIDTH: 100%"/>
   </td>
 </tr>}

end

 @out += "</tbody></table>"
render :text =>@out

end

def zakaz
  @h = {}

  @order = Order.new
  @order.company = params[:zakaz][:company]
  @order.m_phone = params[:zakaz][:m_phone]
  @order.fax = params[:zakaz][:fax]
  @order.mail = params[:zakaz][:mail]
  @order.person = params[:zakaz][:person]
  @order.departure = params[:zakaz][:departure]
  @order.destination = params[:zakaz][:destination]
  @order.date = params[:zakaz][:date]
  @order.flight_num = params[:zakaz][:flight_num]
  @order.flight_time = params[:zakaz][:flight_time]
  @order.arrival_time = params[:zakaz][:arrival_time]
  @order.city = params[:zakaz][:city]
  @order.cars = params[:zakaz][:cars]
  @order.payment = params[:zakaz][:payment]
  @order.dop = params[:zakaz][:dop]
  

  @h = params[:zakaz]
  h_count = @h.find_all{|key, value| key[0..4] == 'name-'}.size

  1.upto(h_count) do |i|
  @order.passenger << Passenger.new(:name => @h["name-#{i}"] , :birthdate => @h["birthdate-#{i}"])
  end
  @order.save
  #redirect_to '/order'
  Mailer.deliver_order(@order)
  render :update do |page|
    page.replace_html "zakaz" , :text => %{<h2>Ваш заказ отправлен! </h2> <br/> <a href="/order"> Отправить еще заказ </a> }

  end

end



end
