class SearchController < ApplicationController
  layout 'index'

  def index

 menu
@page_title ="Корпоративный сайт группы компаний «Живой офис». Поиск по сайту."

    @find_l2 = []
    @find_l3 = []
    @find_smi = []
    @find_press = []

    search=params[:search].strip
    session[:search] = search
    @split = search.downcase.split(' ')

    @cont_l2 = MenuL2.find :all , :conditions=>['vis =? ', 1]
    @cont_l3 = MenuL3.find :all , :conditions=>['vis =?', 1]
    @cont_smi = Smi.find :all , :order=>'created_at desc'
    @cont_press = Pressreliz.find :all , :order=>'created_at desc'
    @cont_news = NewsItem.find :all , :order=>'created_at desc'

            @tmp=@cont_l2
            0.upto(@split.size - 1) do |i|
            @find_l2=@tmp.find_all{|x| x if x.menu_l1.vis and (x.content + ' ' + x.title).downcase.index(@split[i])}
            @tmp=@find_l2
            end


            @tmp=@cont_l3
            0.upto(@split.size - 1) do |i|
            @find_l3=@tmp.find_all{|x| x if x.menu_l2 and  x.menu_l2.vis and x.menu_l2.menu_l1 and x.menu_l2.menu_l1.vis and (x.content + ' ' + x.title).downcase.index(@split[i])}
            @tmp=@find_l3
            end

            @tmp=@cont_smi
            0.upto(@split.size - 1) do |i|
            @find_smi=@tmp.find_all{|x| x if (x.content + ' ' + x.title).downcase.index(@split[i])}
            @tmp=@find_smi
            end

            @tmp=@cont_press
            0.upto(@split.size - 1) do |i|
            @find_press=@tmp.find_all{|x| x if (x.content + ' ' + x.title).downcase.index(@split[i])}
            @tmp=@find_press
            end

            @tmp=@cont_news
            0.upto(@split.size - 1) do |i|
            @find_news=@tmp.find_all{|x| x if (x.content + ' ' + x.title).downcase.index(@split[i])}
            @tmp=@cont_news
            end

  end

end
