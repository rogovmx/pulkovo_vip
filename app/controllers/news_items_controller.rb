class NewsItemsController < ApplicationController
before_filter :authorize
before_filter :redaktor
layout 'admin'

  def index
   @menu = MenuL1.all.sort{|a,b| a <=> b}
    @page_title = "Администрирование. Новости"
    @news_items =  NewsItem.find :all ,:order=>'id desc'

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @news_items }
    end
  end

  # GET /news_items/1
  # GET /news_items/1.xml
  def show
    @menu = MenuL1.all.sort{|a,b| a <=> b}
    @page_title = "Администрирование. Новости"
    @news_item = NewsItem.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @news_item }
    end
  end

  # GET /news_items/new
  # GET /news_items/new.xml
  def new
    @menu = MenuL1.all.sort{|a,b| a <=> b}
    @page_title = "Администрирование. Новости"
    @news_item = NewsItem.new
    @title = "Новость: #{@news_item.title}"

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @news_item }
    end
  end

  # GET /news_items/1/edit
  def edit
    @menu = MenuL1.all.sort{|a,b| a <=> b}
    @page_title = "Администрирование. Новости"
    @news_item = NewsItem.find(params[:id])
  end

  # POST /news_items
  # POST /news_items.xml
  def create
    @news_item = NewsItem.new(params[:news_item])

    respond_to do |format|
      if @news_item.save
        format.html { redirect_to(@news_item, :notice => 'Новость создана.') }
        format.xml  { render :xml => @news_item, :status => :created, :location => @news_item }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @news_item.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /news_items/1
  # PUT /news_items/1.xml
  def update
    @news_item = NewsItem.find(params[:id])

    respond_to do |format|
      if @news_item.update_attributes(params[:news_item])
        format.html { redirect_to(@news_item, :notice => 'Новость сохранена.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @news_item.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /news_items/1
  # DELETE /news_items/1.xml
  def destroy
    @news_item = NewsItem.find(params[:id])
    @news_item.destroy

    respond_to do |format|
      format.html { redirect_to(news_items_url) }
      format.xml  { head :ok }
    end
  end
end
